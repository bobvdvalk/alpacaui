import React, {Component} from 'react';
import Layout from "./components/Layout";
import {Container, Header, Segment, Grid} from "semantic-ui-react";
import SimpleForm from "./components/SimpleForm";

class App extends Component {
    render() {
        return (
            <Layout>
                <Segment padded='very' attached inverted>
                    <Container textAlign='center'>
                        <Header inverted>
                            <h1>Basic App</h1>
                            <Header.Subheader>
                                Wow your crowd with a catchy promise
                            </Header.Subheader>
                        </Header>
                    </Container>
                </Segment>
                <Container className='padded'>

                    <Grid columns="2">
                        <Grid.Column>
                            <Segment>
                                <SimpleForm/>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <SimpleForm/>
                            </Segment>
                        </Grid.Column>
                    </Grid>
                </Container>
            </Layout>
        )
            ;
    }
}

export default App;
