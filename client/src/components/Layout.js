import {Container, Grid, Menu, Segment} from "semantic-ui-react";
import {NavLink} from "react-router-dom";
import React from "react";

export default ({children}) => (
    <div className='app'>
        <Segment className='nav' attached inverted>
            <Container>
                <Menu attached inverted pointing secondary>
                    <Menu.Item name='home'
                               as={NavLink}
                               to='/'/>
                </Menu>
            </Container>
        </Segment>
        <div className='main'>
            {children}
        </div>
        <Segment padded='very' attached inverted>
            <Container>
                <Grid columns={2} stackable>
                    <Grid.Column>
                        <p>Nice App 1.0</p>
                    </Grid.Column>
                    <Grid.Column>
                        <p>
                            The reason we made this app is to do a thing.
                            Here we place a nice description of our missing as an organization.
                        </p>
                    </Grid.Column>
                </Grid>
            </Container>
        </Segment>
    </div>
);
