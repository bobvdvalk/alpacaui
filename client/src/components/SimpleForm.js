import React, {Component} from 'react';
import {Form} from "semantic-ui-react";

export default class SimpleForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            // Je moet dit leeg starten anders doet je form niks (geen 'null')
            symbol: '',
            amount: '',
            timeinforce: ''
        }
    }

    handleChange = (event, component) => {
        // Use the component (Form.Input) name attribute
        this.setState({[component.name]: component.value});
    };

    handleSubmit = () => {
        // Let's send this fool!

        // First we indicate we're busy
        this.setState({loading: true}, () => {
            // Then send the request
            fetch('/forms/formxxx', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    symbol: this.state.symbol,
                    amount: this.state.amount,
                    timeinforce: this.state.timeinforce
                })
            })
            // Convert response to json
                .then(response => response.json())
                .then(data => {
                    // We're done, probably want to do something with this
                    this.setState({loading: false});
                    alert(JSON.stringify(data, 0, 2));
                })

        })
    };

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Input label='Symbol'
                            name='symbol'
                            placeholder='stock symbol'
                            required
                            value={this.state.symbol}
                            onChange={this.handleChange}
                />
                <Form.Input label='Amount'
                            name='amount'
                            placeholder=''
                            value={this.state.amount}
                            onChange={this.handleChange}
                />
                <Form.Input label='Time in time'
                            name='timeinforce'
                            placeholder=''
                            value={this.state.timeinforce}
                            onChange={this.handleChange}
                />
                <Form.Button primary
                             content='Send'
                             loading={this.state.loading}
                             disabled={this.state.loading}
                />
            </Form>
        );
    }
}
